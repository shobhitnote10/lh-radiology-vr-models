import cv2
import numpy as np
from typing import Union, Optional, List, Callable
from video_annotator.VideoProcessor import VideoProcessor
from cv2 import VideoWriter


class ProcessedWriter:
    """
        This class allows writing the images that were processed
            by a model to video-file.
    """
    def __init__(
            self,
            input_path: str,
            output_path: str,
            fps: int = 30,
            fourcc: Union[str, cv2.VideoWriter_fourcc] = cv2.VideoWriter_fourcc(*'MP4V'),
            use_colmap=False,
            colmap: Optional[np.array] = np.array([
                [0, 0, 0], [255, 0, 0], [0, 255, 0], [255, 255, 0],
                [0, 0, 255], [255, 0, 128], [0, 128, 128], [128, 128, 128],
                [64, 0, 0], [192, 0, 0], [64, 128, 0], [192, 128, 0],
                [64, 0, 255], [192, 0, 128], [64, 255, 128], [192, 128, 255],
                [0, 64, 0], [128, 64, 0], [0, 192, 0], [255, 192, 0], [0, 64, 255]
            ]),
            return_orig: bool = False,
            *transforms: Optional[List[Callable]],
    ):
        """
        :param input_path: path to the input video
        :param output_path: path to the resulting video
        :param fps: desired FPS of the video
        :param fourcc: data format's four-character code
        :param use_colmap: True for segmentation and False for detection
        :param colmap: colormap for segmentation
        :param return_orig: return_orig: a boolean, indicating if the original image should be returned
        :param transforms: a list of transforms and/or prediction functions
        """

        self.output_path = output_path
        self.input_path = input_path
        self.return_orig = return_orig
        self.fps = fps
        if isinstance(fourcc, str):
            fourcc = cv2.VideoWriter_fourcc(*fourcc)
        self.fourcc = fourcc
        self.width = None
        self.height = None

        self.processor = VideoProcessor(input_path, self.return_orig, *transforms)
        self.colmap = colmap
        self.use_colmap = use_colmap
        if self.use_colmap:
            assert self.colmap is not None, "Colormap should be provided"

    @property
    def shape(self):
        return self.width, self.height

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close_file()

    def close_file(self):
        self.processor.close_file()
        if self.writer:
            self.writer.release()

    def write_file(self):
        writting_first = True
        with self.processor as processor:
            for frame in processor:
                if self.return_orig:
                    frame = frame[0]
                frame = cv2.cvtColor(np.uint8(frame), code=cv2.COLOR_BGR2RGB)
                if self.width is None:
                    self.height, self.width = frame.shape[:2]

                assert (self.height, self.width) == frame.shape[
                    :2
                ], f'The frame has a shape of {(self.height, self.width)}, but should have: {self.shape}'
                if self.use_colmap:
                    frame = self.colmap[frame, :]
                frame = frame.astype(np.uint8)
                if writting_first:
                    self.writer = VideoWriter(
                        self.output_path,
                        self.fourcc,
                        self.fps,
                        (self.width, self.height),
                    )
                    writting_first = False
                assert self.writer.isOpened(), f"File {self.output_path} is closed"
                self.writer.write(frame)
        self.close_file()
