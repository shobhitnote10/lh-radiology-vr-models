import cv2
import sys
import numpy as np
sys.path.append('../')
sys.path.append('../../')
from typing import Dict
from distorter import *
from VideoProcessor import *
from VideoWriter import ProcessedWriter
from inference.detection import inference_func as detection_inference
from inference.segmentation import inference_func as segmentation_inference


CMAP = np.array([
    [0, 0, 0], [255, 0, 0], [0, 255, 0], [255, 255, 0],
    [0, 0, 255], [255, 0, 128], [0, 128, 128], [128, 128, 128],
    [64, 0, 0], [192, 0, 0], [64, 128, 0], [192, 128, 0],
    [64, 0, 255], [192, 0, 128], [64, 255, 128], [192, 128, 255],
    [0, 64, 0], [128, 64, 0], [0, 192, 0], [255, 192, 0], [0, 64, 255]
])  # put here a colormap for classes

FOURCC = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')


def add_predictions(img: np.ndarray, predictions: Dict, color=(255, 0, 5),
                    thickness=5) -> np.ndarray:
    """
    This function adds bounding-boxes and classnames on image
    :param img: input image
    :param predictions: model predictions in dictionary format
    :param color: bboxes color
    :param thickness: lines thickness
    :return: image with bounding-boxes and classnames added
    """
    for prediction in predictions['predictions']:
        if prediction is not None:
            x, y = int(prediction['x']), int(prediction['y'])
            height, width = prediction['height'], prediction['width']
            true_x, true_y = (int(x - width / 2), int(y + height / 2))
            cv2.rectangle(img, pt1=(true_x, true_y),
                                pt2=(int(x + width / 2), int(y - height / 2)),
                                   color=color, thickness=thickness)
            cv2.putText(img, f"{prediction['class']}: {prediction['confidence']}",
                        org=(true_x + 10, true_y - 10), fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.3,
                        color=color, thickness=1)
    return img


def post_process(frame: np.ndarray) -> np.ndarray:
    """
        This function adds predictions to image
    :param frame: image
    :return: processed image
    """
    preds = detection_inference.predict(frame)
    add_predictions(frame, preds)
    return frame
