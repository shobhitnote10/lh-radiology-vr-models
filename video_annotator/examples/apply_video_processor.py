"""
This program shows how to run VideoProcessor
When you run this program, a new video will be displayed as a slide-show,
    so you can check it frame-by-frame
I recommend using one of my pretrained models (since their predict function is available)
"""

from video_annotator.VideoProcessor import *
from video_annotator.distorter import *
from inference_func import predict  # it can be an inference function of any model you want


if __name__ == "__main__":
    cv2.startWindowThread()
    visibility_coef = 20 # just to make the result easier to see
    with VideoProcessor('Path to your video',
                        preprocess, predict) as v:
        for i in v:
            cv2.imshow('frame', i.astype(np.uint8) * visibility_coef)
            cv2.waitKey(0)  # wait for a keyboard input
            cv2.destroyAllWindows()
