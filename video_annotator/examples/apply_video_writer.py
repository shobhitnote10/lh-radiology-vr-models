"""
This program shows how to run ProcessedWriter from my VideoWriter file
When you run this program, a new video will be saved to the specified file (after application of transformations)
I recommend using one of my pretrained models (since their predict function is available)
"""

import cv2

from video_annotator.VideoWriter import ProcessedWriter
from video_annotator.distorter import *
from inference_func import predict  # it can be an inference function of any model you want


if __name__ == "__main__":
    writer = ProcessedWriter("input video",
                             "output path",
                             30,
                             cv2.VideoWriter_fourcc('m', 'p', '4', 'v'),
                             preprocess, predict)
    writer.write_file()
